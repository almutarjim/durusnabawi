import { createClient } from 'jsr:@supabase/supabase-js@2';

const supabaseUrl = Deno.env.get('SUPABASE_URL') ?? '';
const supabaseAnonKey = Deno.env.get('SUPABASE_ANON_KEY') ?? '';
const supabaseClient = createClient(supabaseUrl, supabaseAnonKey);

const createResponse = (body: any, status = 200) =>
  new Response(JSON.stringify(body), {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization',
      'Content-Type': 'application/json',
    },
    status,
  });

const handleCors = (req: Request) =>
  req.method === 'OPTIONS'
    ? new Response(null, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
          'Access-Control-Allow-Headers': 'Content-Type, Authorization',
        },
        status: 204,
      })
    : undefined;

const getScheduleData = async () => {
  try {
    console.log('Fetching schedules from the database...');

    const { data: schedules, error: schedulesError } = await supabaseClient
      .from('schedules')
      .select(`
        schedule_id,
        lesson_id,
        day,
        time,
        lecturer_id,
        location_id,
        lessons ( lesson_name, topic_id, topics ( topic_name ) ),
        lecturers ( lecturer_name, prefix_id, photo, prefixes ( prefix_name ) ),
        locations ( location_name, section_id, sections ( section_name ), x, y )
      `);

    if (schedulesError) {
      console.error('Error fetching schedules:', schedulesError);
      throw schedulesError;
    }

    console.log('Schedules fetched successfully:', schedules);

    const groupedByLesson = schedules.reduce((acc, schedule) => {
      const lessonId = schedule.lesson_id;
      if (!acc[lessonId]) {
        acc[lessonId] = {
          lesson_name: schedule.lessons?.lesson_name,
          topic_name: schedule.lessons?.topics?.topic_name,
          lecturer: {
            lecturer_name: schedule.lecturers?.lecturer_name,
            prefix: schedule.lecturers?.prefixes?.prefix_name,
            photo: schedule.lecturers?.photo, // Include the new photo field
          },
          location: {
            location_name: schedule.locations?.location_name,
            section: schedule.locations?.sections?.section_name,
            x: schedule.locations?.x,
            y: schedule.locations?.y,
          },
          schedules: [],
        };
      }
      acc[lessonId].schedules.push({
        day: schedule.day,
        time: schedule.time,
      });
      return acc;
    }, {} as { [key: string]: any });

    const compiledData = Object.values(groupedByLesson);

    console.log('Compiled data:', compiledData);

    return createResponse(compiledData);
  } catch (error) {
    console.error('Error processing schedules:', error);
    return createResponse({ message: 'Error', details: error.message }, 500);
  }
};

const handleRequest = async (req: Request) => {
  if (handleCors(req)) return handleCors(req);

  if (req.method === 'GET') {
    return getScheduleData();
  }

  return createResponse({ message: 'Method not allowed', details: 'Only GET and OPTIONS methods are supported' }, 405);
};

Deno.serve(handleRequest);