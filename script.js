const fetchEndpoint = 'https://izedowmqodfiouvaufso.supabase.co/functions/v1/fetch';

const fetchData = async () => {
    try {
        console.log('Fetching schedule data...');
        const response = await fetch(fetchEndpoint);
        if (!response.ok) throw new Error(response.statusText);
        return await response.json();
    } catch (error) {
        console.error('Error:', error);
        return [];
    }
};

fetchData().then(data => {
    console.log(data);
});

const generateUI = (data) => {
    const container = document.getElementById('cards');
    container.innerHTML = data.map(({ lesson_name, topic_name, lecturer: { lecturer_name, prefix = '', photo }, location: { location_name, x, y }, schedules }) => {
        const allSameTime = schedules.every(s => s.time === schedules[0].time);
        const daysHTML = `<div class="desc-days row">${schedules.map(s => `<div class="day-chip chip" data-type="day" data-value="${s.day}">${allSameTime ? s.day : `${s.day}: ${s.time}`}</div>`).join('')}</div>${allSameTime ? `<div class="desc-time"><img src="clock.svg">&nbsp;${schedules[0].time}</div>` : ''}`;
        
        return `
            <div class="card col" data-schedules='${JSON.stringify(schedules)}'>
                <div class="title col" style="background-image: linear-gradient(to top, var(--md-sys-color-surface-container-low), transparent), url('images/photos/${photo}.jpg');">
                    <div class="title-lesson">${lesson_name}</div>
                    <div class="title-lecturer">الشيخ<span class="prefix">${prefix}</span> ${lecturer_name}</div>
                    <div class="topic chip">${topic_name}</div>
                </div>
                <div class="details row">
                    <div class="description col">${daysHTML}</div>
                    <img class="location" src="loc.svg" data-x="${x}" data-y="${y}" data-lesson-name="${lesson_name}" data-lecturer-name="${lecturer_name}" data-location-name="${location_name}">
                </div>
            </div>
        `;
    }).join('');

    document.querySelectorAll('.location').forEach(img => img.addEventListener('click', () => showMap(img)));
    document.querySelectorAll('.filter-chip').forEach(chip => chip.addEventListener('click', () => applyFilter(chip)));
};

const applyFilter = (chip) => {
    chip.classList.toggle('active');

    const activeFilters = Array.from(document.querySelectorAll('.filter-chip.active'))
        .reduce((filters, chip) => {
            const { type, value } = chip.dataset;
            (filters[type] ||= []).push(value);
            return filters;
        }, {});

    document.querySelectorAll('.card').forEach(card => {
        const schedules = JSON.parse(card.dataset.schedules);
        const matchesAllFilters = Object.entries(activeFilters).every(([type, values]) => {
            return schedules.some(schedule => values.includes(schedule[type]));
        });

        card.style.display = matchesAllFilters ? 'block' : 'none';
    });
};

const showMap = (img) => {
    const { x, y, lessonName, lecturerName, locationName } = img.dataset;
    const map = document.querySelector('.map');
    const container = document.querySelector('.container');
    const [mapTitle, mapLocationDetail] = ['map-title', 'map-location-detail']
        .map(selector => document.querySelector(`.${selector}`));

    // Set CSS variables on the container
    container.style.setProperty('--xcoor', `${x}%`);
    container.style.setProperty('--ycoor', `${y}%`);

    // Update text content
    mapTitle.textContent = `${lessonName} - ${lecturerName}`;
    mapLocationDetail.textContent = locationName;

    // Display the map
    map.style.display = 'flex';

    // Hide the map when clicked outside
    map.addEventListener('click', (e) => {
        if (e.target === map) map.style.display = 'none';
    }, { once: true });
};

const main = async () => {
    const data = await fetchData();
    generateUI(data);
};

document.addEventListener('DOMContentLoaded', main);